## Dependencies

* [pipenv](https://pipenv.pypa.io/en/latest/install/#installing-pipenv)

## Installation

1. `pipenv install`
2. `pipenv shell`
3. `cd dtesting`
4. `python manage.py migrate`
5. `python manage.py test`